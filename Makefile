release: all
	psp-fixup-imports -m $(PSP_FW_VERSION)mapfile.txt -o $(TARGET)631.prx $(TARGET).prx
	psp-packer $(TARGET).prx
	psp-packer $(TARGET)631.prx
	
TARGET = libconfig
OBJS = src/main.o src/config.o

INCDIR = include
CFLAGS = -Os -G0 -Wall -fno-pic -fshort-wchar
ASFLAGS = $(CFLAGS)

BUILD_PRX = 1
PRX_EXPORTS = src/exports.exp

PSP_FW_VERSION = 631

USE_KERNEL_LIBS=1
USE_KERNEL_LIBC=1

LIBDIR = ../lib
LDFLAGS = -nostartfiles
LIBS = 

PSPSDK=$(shell psp-config --pspsdk-path)
include $(PSPSDK)/lib/build.mak

